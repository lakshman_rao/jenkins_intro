package com.dd.labattula.contactapp;

import android.content.pm.PackageManager;

/**
 * Created by labattula on 08/01/16.
 */
public class PermissionUtils {

    public static boolean verifyPermissions(int[] grantresults) {

        if (grantresults.length < 1) {
            return false;
        }

        for (int result : grantresults) {

            if (result != PackageManager.PERMISSION_GRANTED) {
                return false;
            }

        }

        return true;
    }
}
