package com.dd.labattula.contactapp;

import android.Manifest;
import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.dd.labattula.contactapp.adapter.MyRecycleAdapter;
import com.dd.labattula.contactapp.models.DataObject;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        com.google.android.gms.location.LocationListener {

    private final static String TAG = "MapActivity";
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 10;

    private GoogleMap mMap;

    private GoogleApiClient mGoogleApiClient;

    private final String[] LOCATION_PERMISSIONS = {Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION};

    private final int LOCATION_REQUEST = 1;

    private View mLayout;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;

    private LocationRequest mLocationRequest;

    private RecyclerView mRecycleView;

    private MyRecycleAdapter adapter = null;

    private RecyclerView.LayoutManager mLayoutManager;

    private DataObject currentPlace = null;
    ArrayList<DataObject> dataList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!ServicesUtils.isServicesAvailable(this)) {
            finish();
        }

        setContentView(R.layout.activity_maps);

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mLayout = findViewById(R.id.map_layout);
        mRecycleView = (RecyclerView) findViewById(R.id.sampleRecycleView);

        mLayoutManager = new LinearLayoutManager(this);

        buildApiClient();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        // client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();

        mLocationRequest = LocationRequest.create().setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(10 * 1000).setFastestInterval(1 * 1000);


        CollapsingToolbarLayout collapsingToolbarLayout =
                (CollapsingToolbarLayout) findViewById(R.id.collapsable_tool_bar);


        collapsingToolbarLayout.setTitle("Sample title");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().
                getColor(android.R.color.transparent));

        collapsingToolbarLayout.setContentScrimColor(Color.BLUE);
        collapsingToolbarLayout.setStatusBarScrimColor(Color.GREEN);

        setRecycleAdapter();
    }

    private void setRecycleAdapter() {
        adapter = new MyRecycleAdapter();
        adapter.setDataObjects(getDataObjects());

        mRecycleView.setLayoutManager(mLayoutManager);
        mRecycleView.setAdapter(adapter);
    }


    private ArrayList<DataObject> getDataObjects() {
        dataList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            DataObject object = new DataObject();
            object.setFirstTxt("Data." + (i + 1));
            object.setSecondTxt("Data." + (i + 1));
            object.setAdditionTime(Calendar.getInstance().getTimeInMillis());
            dataList.add(object);
        }
        Collections.reverse(dataList);
        return dataList;
    }

    private void buildApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this).
                addConnectionCallbacks(this).addOnConnectionFailedListener(this).
                addApi(LocationServices.API).addApi(Places.PLACE_DETECTION_API)
                .build();
    }

    private void requestLocationPermission() {
        showLog("request location permission");

        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

            Log.i(TAG,
                    "Displaying camera permission rationale to provide additional context.");
            Snackbar.make(mLayout, R.string.permission_location_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat.requestPermissions(MapsActivity.this,
                                    LOCATION_PERMISSIONS,
                                    LOCATION_REQUEST);
                        }
                    })
                    .show();
        } else {
            ActivityCompat.requestPermissions(MapsActivity.this, LOCATION_PERMISSIONS, LOCATION_REQUEST);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        //setupMapIfNeeded();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mGoogleApiClient.disconnect();
        }
    }

//    private void setUpMap() {
//        mMap.addMarker(new MarkerOptions().position(new LatLng(0, 0)).title("Marker"));
//    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                Snackbar.make(mLayout, "Map click", Snackbar.LENGTH_SHORT).show();
                if (mRecycleView.getVisibility() == View.VISIBLE) {
                    mRecycleView.animate()
                            .translationY(mRecycleView.getHeight())
                            .alpha(0.0f)
                            .setDuration(300)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    mRecycleView.setVisibility(View.GONE);
                                }
                            });
                } else {
                    mRecycleView.animate()
                            .translationY(mRecycleView.getHeight())
                            .alpha(1.0f)
                            .setDuration(300)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    mRecycleView.setVisibility(View.VISIBLE);
                                }
                            });
                }
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
                ) {
            Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
//            if (location == null) {
            showLog("location available");
            handleNewLocation(location);
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);

            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi.getCurrentPlace(
                    mGoogleApiClient, null);

            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(PlaceLikelihoodBuffer placeLikelihoods) {
                    try {
                        if (placeLikelihoods != null) {
                            PlaceLikelihood buffer = placeLikelihoods.get(0);

                            currentPlace = new DataObject();
                            currentPlace.setFirstTxt(buffer.getPlace().getAddress().toString());
                            currentPlace.setSecondTxt(buffer.getPlace().getName().toString());
                            currentPlace.setAdditionTime(Calendar.getInstance().getTimeInMillis());

                            //setRecycleAdapter();

                            dataList.add(0,currentPlace);
                            adapter.notifyDataSetChanged();

                            placeLikelihoods.release();
                        }
                    } catch (IllegalStateException e) {
                        Log.i(TAG, "GPS Not Turned on" + e);
                    }
                }
            });

//            } else {
//                handleNewLocation(location);
//            }
        } else {
            requestLocationPermission();
        }

    }

    private void handleNewLocation(Location location) {
        showLog("latitude.." + location.getLatitude() + "..longitude.." + location.getLongitude());
        LatLng myLocation = new LatLng(location.getLatitude(), location.getLongitude());
        mMap.addMarker(new MarkerOptions().position(myLocation).title("My Location"));
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(myLocation));

        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 10.0f));

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == LOCATION_REQUEST) {

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

        showLog("onConnectionSuspended");

    }

    @Override

    public void onConnectionFailed(ConnectionResult connectionResult) {
        showLog("onConnectionFailed");

        if (connectionResult.hasResolution()) {
            try {
                // Start an Activity that tries to resolve the error
                connectionResult.startResolutionForResult(this, CONNECTION_FAILURE_RESOLUTION_REQUEST);
            } catch (IntentSender.SendIntentException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(TAG, "Location services connection failed with code " + connectionResult.getErrorCode());
        }

    }

    private void showLog(String str) {
        Log.i(TAG, "showLog: " + str);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onLocationChanged(Location location) {
        handleNewLocation(location);
    }
}
