package com.dd.labattula.contactapp;

import android.app.Activity;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;

/**
 * Created by labattula on 08/01/16.
 */
public class ServicesUtils {

    public static boolean isServicesAvailable(Activity mActivity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mActivity);

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(resultCode, mActivity, 0).show();
            return false;
        }
    }
}
