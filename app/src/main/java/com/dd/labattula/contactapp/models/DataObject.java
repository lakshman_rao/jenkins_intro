package com.dd.labattula.contactapp.models;

import java.util.Comparator;

/**
 * Created by labattula on 23/01/16.
 */
public class DataObject implements Comparable{

    private String firstTxt;
    private String secondTxt;
    private long additionTime;

    public long getAdditionTime() {
        return additionTime;
    }

    public void setAdditionTime(long additionTime) {
        this.additionTime = additionTime;
    }

    public String getFirstTxt() {
        return firstTxt;
    }

    public void setFirstTxt(String firstTxt) {
        this.firstTxt = firstTxt;
    }

    public String getSecondTxt() {
        return secondTxt;
    }

    public void setSecondTxt(String secondTxt) {
        this.secondTxt = secondTxt;
    }

    @Override
    public int compareTo(Object another) {
        DataObject obj = (DataObject)another;
        if(getAdditionTime()>obj.getAdditionTime())
            return 0;
        return 1;
    }
}
