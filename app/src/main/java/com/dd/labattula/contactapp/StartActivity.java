package com.dd.labattula.contactapp;

import android.Manifest;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

//import com.afollestad.materialdialogs.DialogAction;
//import com.afollestad.materialdialogs.MaterialDialog;
//import com.afollestad.materialdialogs.color.ColorChooserDialog;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.places.ui.PlacePicker;
import com.subinkrishna.widget.CircularImageView;

public class StartActivity extends AppCompatActivity
//        implements ColorChooserDialog.ColorCallback
{

    private static final String TAG = "StartActivity";

    private static final int REQUEST_CONTACT = 1;

    private static String[] PERMISSION_CONTACT = {Manifest.permission.READ_CONTACTS,
            Manifest.permission.WRITE_CONTACTS};

    private View mLayout;

    private Button mMapButton;
    private Button mPlacesButton;
    private Button mDialogButton;
    private Button mColorButton;
    private Button mMapDetailsButton;
    private Button mCoordinateButton;

    private CircularImageView circularImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkServicesAvailability();

        setContentView(R.layout.activity_start);

        mLayout = findViewById(R.id.helloTxt);

        mMapButton = (Button) findViewById(R.id.launch_map);
        mPlacesButton = (Button) findViewById(R.id.launch_places);
        mDialogButton = (Button) findViewById(R.id.dialogBtn);
        mColorButton = (Button) findViewById(R.id.colorBtn);
        mMapDetailsButton = (Button) findViewById(R.id.launchMapFragment);
        mCoordinateButton = (Button) findViewById(R.id.launchCoordinate);

        circularImageView = (CircularImageView) findViewById(R.id.launchCircleImage);

        circularImageView.setBorderColor(Color.RED);
        circularImageView.setBorderWidth(TypedValue.COMPLEX_UNIT_DIP, 2);
        circularImageView.setPlaceholder("CV", Color.GREEN, Color.BLUE);
        circularImageView.setPlaceholderTextSize(TypedValue.COMPLEX_UNIT_SP, 22);

        mMapDetailsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    PlacePicker.IntentBuilder builder = new PlacePicker.IntentBuilder();
                    Intent intent = builder.build(StartActivity.this);
                    startActivityForResult(intent, 0);
                } catch (GooglePlayServicesRepairableException e) {

                } catch (GooglePlayServicesNotAvailableException e) {

                }
            }
        });

        mCoordinateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        mMapButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mapIntent = new Intent();
                mapIntent.setClass(StartActivity.this, MapsActivity.class);
                startActivity(mapIntent);
            }
        });

        mPlacesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent placesIntent = new Intent();
                placesIntent.setClass(StartActivity.this, PlacesActivity.class);
                startActivity(placesIntent);
            }
        });

        mDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchDialog();
            }
        });

        mColorButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launcgColorDialog();
            }
        });
        showContacts(null);
    }

    private void launcgColorDialog() {
        boolean accent = true;

//        new ColorChooserDialog.Builder(this, R.string.color_palette)
//                .titleSub(R.string.colors)  // title of dialog when viewing shades of a color
//                .accentMode(accent)  // when true, will display accent palette instead of primary palette
//                .doneButton(R.string.md_done_label)  // changes label of the done button
//                .cancelButton(R.string.md_cancel_label)  // changes label of the cancel button
//                .backButton(R.string.md_back_label)  // changes label of the back button
//                .preselect(accent ? getResources().getColor(R.color.accentPreselect) : getResources().getColor(R.color.primaryPreselect))  // optionally preselects a color
//                .dynamicButtonColor(true)  // defaults to true, false will disable changing action buttons' color to currently selected color
//                .show();
    }

    private void launchDialog() {
//        new MaterialDialog.Builder(this)
//                .title(R.string.dialog_launch)
//                .content(R.string.content)
//                .positiveText(R.string.positive)
//                .negativeText(R.string.negative)
//                .onPositive(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//
//                    }
//                })
//                .onNegative(new MaterialDialog.SingleButtonCallback() {
//                    @Override
//                    public void onClick(MaterialDialog dialog, DialogAction which) {
//
//                    }
//                }).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode == REQUEST_CONTACT) {

        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void checkServicesAvailability() {
        if (!isServicesAvailable()) {
            finish();
        } else {
            showLog("Play Services updated needed");
        }
    }

    /**
     * show contacts view
     *
     * @param v
     */
    private void showContacts(View v) {
        showLog("showing contacts");

        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED)
                || (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CONTACTS))
                != PackageManager.PERMISSION_GRANTED) {
            showLog("show contacts permission denied");
            //request permissions
            requestContactPermissions();
        } else {
            showContactDetails();
        }
    }

    private final static String[] PROJECTION = {ContactsContract.Contacts._ID,
            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY};

    private final static String ORDER = ContactsContract.Contacts.DISPLAY_NAME + " ASC";

    /**
     * Showing the contact details
     */
    private void showContactDetails() {
        Snackbar.make(mLayout, "Show contact details here", Snackbar.LENGTH_SHORT).show();

        getLoaderManager().restartLoader(REQUEST_CONTACT, null, new LoaderManager.LoaderCallbacks<Cursor>() {
            @Override
            public Loader<Cursor> onCreateLoader(int id, Bundle args) {
                //return null;
                return new CursorLoader(StartActivity.this, ContactsContract.Contacts.CONTENT_URI, PROJECTION, null, null, ORDER);
            }

            @Override
            public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
                if (data != null) {
                    final int totalCount = data.getCount();
                    if (totalCount > 0) {
                        data.moveToFirst();
                        String name = data.getString(data.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                        showLog("First contact Name is .." + name);
                        showLog("Total count is.." + totalCount);
                    }
                }
            }

            @Override
            public void onLoaderReset(Loader<Cursor> loader) {
                showLog("Contact list is empty");
            }
        });
    }

    /**
     * Requesting the ContactPermissions
     */
    private void requestContactPermissions() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CONTACTS)
                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_CONTACTS)) {
            // Provide an additional rationale to the user if the permission was not granted
            // and the user would benefit from additional context for the use of the permission.
            // For example, if the request has been denied previously.
            Log.i(TAG, "Displaying contacts permission rationale to provide additional context.");

            // Display a SnackBar with an explanation and a button to trigger the request.
            Snackbar.make(mLayout, R.string.permission_contacts_rationale,
                    Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ActivityCompat
                                    .requestPermissions(StartActivity.this, PERMISSION_CONTACT,
                                            REQUEST_CONTACT);
                        }
                    }).show();
        } else {
            //permissions grant yet so request the permissions
            ActivityCompat.requestPermissions(this, PERMISSION_CONTACT, REQUEST_CONTACT);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_start, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isServicesAvailable() {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (ConnectionResult.SUCCESS == resultCode) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(resultCode, this, 0).show();
            return false;
        }
    }

    private void showLog(String str) {
        Log.i(TAG, "showLog: " + str);
    }

//    @Override
//    public void onColorSelection(ColorChooserDialog dialog, int selectedColor) {
//        Snackbar.make(mLayout, "Color is" + selectedColor, Snackbar.LENGTH_SHORT).show();
//        mColorButton.setBackgroundColor(selectedColor);
//    }
}
