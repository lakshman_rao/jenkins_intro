package com.dd.labattula.contactapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dd.labattula.contactapp.R;
import com.dd.labattula.contactapp.models.DataObject;

import java.util.ArrayList;

/**
 * Created by labattula on 23/01/16.
 */
public class MyRecycleAdapter extends RecyclerView.Adapter<MyRecycleAdapter.MyViewHolder> {

    ArrayList<DataObject> myData = null;

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycleview,
                parent, false);

        MyViewHolder viewHolder = new MyViewHolder(view);

        return viewHolder;
    }

    public void setDataObjects(ArrayList<DataObject> sampleData) {
        myData = sampleData;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.firstTextView.setText(myData.get(position).getFirstTxt());
        holder.secondTextView.setText(myData.get(position).getSecondTxt());
    }

    @Override
    public int getItemCount() {
        return myData.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView firstTextView;
        TextView secondTextView;

        public MyViewHolder(View itemView) {
            super(itemView);
            firstTextView = (TextView) itemView.findViewById(R.id.txt_first);
            secondTextView = (TextView) itemView.findViewById(R.id.txt_second);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
